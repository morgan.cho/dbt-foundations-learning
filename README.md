# dbt Foundations Learning

This project is here to assist in Git, GitLab, and dbt education at PitchBook.

# Minimum Requirements Walkthrough


<details><summary>1. Setting up Git and Gitlab</summary>

### Git
Type the below into terminal to verify if you have git installed:

``` git --version```

if not, a prompt will pop up with the option to install. Click install.

Once installation is complete, follow the guide below to configure git:
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-https


### GitLab
#### Setting up your SSH Key

Start by generating an SSH Key from your terminal–type or copy the below into your terminal:

```ssh-keygen -t ed25519 -C "<comment>"```

The comment portion is optional and for your reference only–add a personal remark or name to the key if desired, ie. "gitlab_key".

You'll be prompted to change the filename and/or directory. Accept the default provided.

You'll then be prompted to enter a passphrase for your key. It is optional but recommended to set one. The passphrase may be updated with the following command in the future:

```ssh-keygen -p -f /path/to/ssh_key```

#### Adding the SSH key to your GitLab profile

Run this command in terminal to copy your public key:

```tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy```

Now, sign in to GitLab through your okta dashboard or shortcut ->
On the left sidebar, select your avatar ->
Select Edit profile ->
On the left sidebar, select SSH Keys ->
Select Add new key.

If any issues occur with adding the key, refer to this link:
https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account

Additonal documentation here: https://docs.gitlab.com/ee/user/ssh.html

</details>

<details><summary>2. Connecting Snowflake to VS Code (IDE)</summary>

### Snowflake Setup
Open VS Code and go to the Extensions tab.

Search for "Snowflake" and download the verified extension.
Once complete, a Snowflake icon will appear in the sidebar located to the left. The interface will include fields that will be necessary to connect to the marketing databases.

Copy this URL and paste it into the initial prompt:

    http://pitchbook-pitchbook.snowflakecomputing.com/

Next, on the dropdown, select Single Sign-On and enter your PitchBook email into the Username field.

Select Open on the prompt that pops up. If the credentials were accepted, you'll be redirected to a white page with the following prompt:

    Your identity was confirmed and propagated to Snowflake Node.js driver. You can close this window now and go back where you started from.

You may now enter your database (ie. INTERNAL) and your schema (ie. MARKETING)

Test run some scripts to confirm proper setup.

</details>

<details><summary>3. Setting up Python</summary>

### Installing Python
Now that you have homebrew installed, you can easily install python onto your device. Open terminal and paste in the following code to see which Python versions are available:

    brew search python | grep python@
We're currently working with version 3.9, so again in terminal, paste the following code to install it:

    brew install python@3.9

We can also install multiple versions and change which one is active with:

    brew link python@3.whatever

For further reference you can reach out to an SDE, such as Steven Kessler, for clarifications on best practices.

</details>

# Local Development Setup

## Installation Requirements

### pyenv

pyenv is a tool that lets you easily manage multiple installations of python. We are setting up this project to work with a specific version of python, and we want to be sure that we can manage local dependencies while not conflicting with your global environment. To learn more about pyenv, check out their [GitHub repo](https://github.com/pyenv/pyenv?tab=readme-ov-file#understanding-python-version-selection).

You can install pyenv via Homebrew:

```
brew update
brew install pyenv
```

<details><summary>Using pyenv</summary>

Once pyenv is installed, if the python version is not corresponding to the version you would like, run the following example provided:
    
    # Skeleton: Pyenv local <Python Version #>
	pyenv local 3.8.18
	
Activating virtualenv with pyenv:
	
    # Skeleton: Pyenv virtualenv <venv name>
	pyenv virtualenv venv
	
    # Skeleton: Pyenv activate <venv name specified above>
	pyenv activate venv

Deactivating the virtualenv
    
    pyenv deactivate

</details>

<details><summary>Troubleshooting: Adding pyenv to PATH if Homebrew failed to install it properly</summary>

You should then add pyenv to your PATH using the following commands:

```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo '[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
```

Once this is done, you should open a new shell and close the current one.
</details>

### poetry

poetry is a tool that helps you manage local python packages and dependency management for the projects that you use.  It also helps build those dependencies in a virtual envirionment so we can all be working out of the same environment.  If you want to learn more about poetry check out their [documentation](https://python-poetry.org/).

To install poetry, we will again leverage homebrew:


```
brew install poetry
```
Homebrew should also add the poetry command to your PATH. To double check, enter the following command:
```
poetry --version
```
If the version is specified, the installation was a success.

### environmental variables

We will want to pass some user specific variables to the VM to help us run dbt with context:

*WARNING: be sure to replace your actual pitchbook email address and team before you run these commands*

```
echo 'export SNOWFLAKE_USER="<FIRST NAME>.<LAST NAME>@PITCHBOOK.COM"' >> ~/.zshrc
echo 'export SNOWFLAKE_TEAM="<MARKETING/REVOPS>"' >>  ~/.zshrc
```

# Global Development Setup

## Setting up the DBT environment

### Cloning the Repository
We need to head to this link to clone our project repository:
https://gitlab.com/pitchbook/marketing-data-science/data-pipelines/dbt-marketing-analytics

Click on the blue 'code' button and you'll see a 'Clone with SSH' option. Click the 'Copy URL' button to the right and open up Terminal.
Run the following commands in order to clone the repo:

    # Navigate to your home directory
    cd $HOME

    # Create the directory to house your cloned repository
    mkdir parent

    # Switch to that directory
    cd parent

    # Clone the repo into the directory
    git clone <INSERT LINK COPIED FROM URL TO CLONE WITH SSH>

These commands will clone the specified repo into your parent project directory. 

Refer to this documentation for further reference:
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-https

If any issues occur with the SSH key/cloning process, refer to the 'Adding the SSH key to your GitLab profile' in step 1 of the Minimum Requirements Walkthrough.

### Initialize Environment

We now want to make sure our environment is set up to run dbt (each command should be run individually):

1. Navigate to base project
```
cd ~
cd parent/dbt-marketing-analytics
```
2. Install Python 3.8 (version being utilized for this project)
```
pyenv install 3.8.18
```
3. Tell poetry to use that python version
```
poetry env use 3.8.18
```
4. Install local dependencies
```
poetry install
```
5. Navigate to dbt_project & initiate VM
```
cd core/dbt_project
poetry shell
```
6. Verify dbt version
```
dbt --version
```
7. Exit the VM
```
exit
```